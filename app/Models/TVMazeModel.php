<?php

namespace App\Models;


class TVMazeModel
{

	/**
	 * The attributes that are assignable for URL assembly.
	 *
	 */
	// The root url	
    public $urlAPI= 'http://api.tvmaze.com';
    // search method at TVMaze
    public $searchMethod='search';
    // search show method at TVMaze
    public $showSearch='shows';   
}
