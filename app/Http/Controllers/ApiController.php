<?php

namespace App\Http\Controllers;

use Input;
use Response;
use App\Models\TVMazeModel;
use App\Models\TVMazeShowModel;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\Exception\RequestException;


class ApiController extends Controller
{
	/**
     * Creates a new instance of the controller with a new instance of the model
     *
     */
    protected $model;

	public function __construct(TVMazeModel $model) {
		$this->model=$model;
	}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search() {
        //Get query string;
        $query=Input::get('q','');
        //URL TVMaze assembly
        $url=$this->model->urlAPI.'/'.$this->model->searchMethod.'/'.$this->model->showSearch.'?q='.$query;

        //Create client http
        $client = new Guzzle();
        //Get request
        try {
           $response = $client->request('GET', $url);
        } catch (RequestException $e) {
            return response($e->getResponse()->getBody(), $e->getResponse()->getStatusCode())
                  ->header('Content-Type', 'application/json');
        }            
        //Get body response and parse json
        if($response->getStatusCode()==200){
            $shows=json_decode($response->getBody());
            //Create results array
            $results=array();
            foreach ($shows as $show) {
                //Lowercase string compare for case unsensitive
                if(strpos(strtolower($show->show->name), strtolower($query))!==FALSE){
                    //Create Show Model
                    $objShow= new TVMazeShowModel;
                    $objShow->score=$show->score;
                    $objShow->name=$show->show->name;
                    //Assign to result
                    $results[]=$objShow;
                }
            }
                    
            return Response::json($results);
        }
        else{
             return response('{"error":"TVMaze Error", "status":"'.$response->getStatusCode().'"}', $response->getStatusCode())->header('Content-Type', 'application/json');
        }
    }
}
